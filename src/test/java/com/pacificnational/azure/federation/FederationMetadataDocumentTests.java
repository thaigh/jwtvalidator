package com.pacificnational.azure.federation;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by tyler on 21/7/17.
 */
public class FederationMetadataDocumentTests {

    private static final String XmlFederationMetadataDocument =
        "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>" +
        "<EntityDescriptor>" +
        "    <IDPSSODescriptor>" +
        "        <KeyDescriptor use=\"signing\">" +
        "            <KeyInfo>" +
        "                <X509Data>" +
        "<X509Certificate>" +
        "MIIDBTCCAe2gAwIBAgIQZSAeaqWig4BHC1ksmNNcgjANBgkqhkiG9w0BAQsFADAtMSswKQYDVQQDEyJhY2NvdW50cy5hY2Nlc3Njb250cm9sLndpbmRvd3MubmV0MB4XDTE3MDUwNjAwMDAwMFoXDTE5MDUwNzAwMDAwMFowLTErMCkGA1UEAxMiYWNjb3VudHMuYWNjZXNzY29udHJvbC53aW5kb3dzLm5ldDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJL7dVZkeJ8JDTPIzFNMJo9oEkvDc52zrdqWSOmex0E/3rBe1tfRmUe2O9l6NYuc10QzPaNxnkyIwa5698suNUPxGlEOwvm/h5oPeFcuxAWcgYYv5s589HWFrNjCF8EfOjZ4vU5oE1744EzxdmBiaGbeE8HDBXn1vK4owlcjawgQNF7KQ6LjSWr7xtARDF2IoKE4RYy0V0uVN4eAQ3zWRWWUt3cpYjTl34EZ9pO6CE8/If7noj75g0RYd/AHkIMdXBC4SlnNz+boLbba2ksIB+5z9jVILjvZwqx7i+k2filqZkVmasB/o2ChUFbdwg6c8DTOlyS/qB25dcfoCN2GF+cCAwEAAaMhMB8wHQYDVR0OBBYEFGKpXQNrF5IoxS6bL4F92+gxOJlIMA0GCSqGSIb3DQEBCwUAA4IBAQA3HgW5SoHlvvQVxqqi+mtscDZLhNfe13iG/nx8Er5il82b79RVydNs+f9sYxc4T4ctnrZu7x5e7jInJedNdAlrPorBdw+SfvKJsmiNndXugMew1FlcQTQVIFDCbziaJav8rKyMxPfeKkc1aixbajWZkKg6OPmmJn2ceTocbn8PMQy20xNvcWUwgF5FZZIuPqu6feOLJcUIYw+0JFZ265xka30QXpmytcIxajIzpD4PRdCIBuVSqgXacAs4t4+w+OhnosD72yvXck8M4GwX1j+vcuyw0yhDGNMmqsHWP7H3jnJiGDrKhhdVyplzDhTfv2Whbv/dIDn+meLE3yyC5yGL" +
        "</X509Certificate>" +
        "                </X509Data>" +
        "            </KeyInfo>" +
        "        </KeyDescriptor>" +
        "    </IDPSSODescriptor>" +
        "</EntityDescriptor>";

    private static final String CertificateRawString = "MIIDBTCCAe2gAwIBAgIQZSAeaqWig4BHC1ksmNNcgjANBgkqhkiG9w0BAQsFADAtMSswKQYDVQQDEyJhY2NvdW50cy5hY2Nlc3Njb250cm9sLndpbmRvd3MubmV0MB4XDTE3MDUwNjAwMDAwMFoXDTE5MDUwNzAwMDAwMFowLTErMCkGA1UEAxMiYWNjb3VudHMuYWNjZXNzY29udHJvbC53aW5kb3dzLm5ldDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJL7dVZkeJ8JDTPIzFNMJo9oEkvDc52zrdqWSOmex0E/3rBe1tfRmUe2O9l6NYuc10QzPaNxnkyIwa5698suNUPxGlEOwvm/h5oPeFcuxAWcgYYv5s589HWFrNjCF8EfOjZ4vU5oE1744EzxdmBiaGbeE8HDBXn1vK4owlcjawgQNF7KQ6LjSWr7xtARDF2IoKE4RYy0V0uVN4eAQ3zWRWWUt3cpYjTl34EZ9pO6CE8/If7noj75g0RYd/AHkIMdXBC4SlnNz+boLbba2ksIB+5z9jVILjvZwqx7i+k2filqZkVmasB/o2ChUFbdwg6c8DTOlyS/qB25dcfoCN2GF+cCAwEAAaMhMB8wHQYDVR0OBBYEFGKpXQNrF5IoxS6bL4F92+gxOJlIMA0GCSqGSIb3DQEBCwUAA4IBAQA3HgW5SoHlvvQVxqqi+mtscDZLhNfe13iG/nx8Er5il82b79RVydNs+f9sYxc4T4ctnrZu7x5e7jInJedNdAlrPorBdw+SfvKJsmiNndXugMew1FlcQTQVIFDCbziaJav8rKyMxPfeKkc1aixbajWZkKg6OPmmJn2ceTocbn8PMQy20xNvcWUwgF5FZZIuPqu6feOLJcUIYw+0JFZ265xka30QXpmytcIxajIzpD4PRdCIBuVSqgXacAs4t4+w+OhnosD72yvXck8M4GwX1j+vcuyw0yhDGNMmqsHWP7H3jnJiGDrKhhdVyplzDhTfv2Whbv/dIDn+meLE3yyC5yGL";

    @Test
    public void testGetDocumentAsString() throws Exception {
        FederationMetadataDocument doc = new FederationMetadataDocument(XmlFederationMetadataDocument);
        assertEquals(XmlFederationMetadataDocument, doc.getDocumentAsString());
    }

    @Test
    public void testGetAllCertificates_ZeroCertificates_EmptyList() throws Exception {
        String _xmlFederationMetadataDocument =
                "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<EntityDescriptor>" +
                "    <IDPSSODescriptor>" +
                "        <KeyDescriptor use=\"signing\">" +
                "            <KeyInfo>" +
                "            </KeyInfo>" +
                "        </KeyDescriptor>" +
                "    </IDPSSODescriptor>" +
                "</EntityDescriptor>";

        FederationMetadataDocument doc = new FederationMetadataDocument(_xmlFederationMetadataDocument);
        List<FederationCertificate> certs = doc.getAllCertificates();
        assertEquals(0, certs.size());
    }

    @Test
    public void testGetAllCertificates_OneCertificate() throws Exception {
        FederationMetadataDocument doc = new FederationMetadataDocument(XmlFederationMetadataDocument);
        List<FederationCertificate> certs = doc.getAllCertificates();
        assertEquals(1, certs.size());

        String certString = addCertHeaders(CertificateRawString);
        assertEquals(certString, certs.get(0).getCertificate());
    }

    private String addCertHeaders(String rawCert) {
        String certString =
                "-----BEGIN CERTIFICATE-----\n" +
                rawCert +
                "\n-----END CERTIFICATE-----";
        return certString;
    }

    @Test
    public void testGetAllCertificates_TwoCertificates() throws Exception {
        String _xmlFederationMetadataDocument =
                "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>" +
                "<EntityDescriptor>" +
                "    <IDPSSODescriptor>" +


                "        <KeyDescriptor use=\"signing\">" +
                "            <KeyInfo>" +
                "                <X509Data>" +
                "<X509Certificate>" +
                "MIIDBTCCAe2gAwIBAgIQZSAeaqWig4BHC1ksmNNcgjANBgkqhkiG9w0BAQsFADAtMSswKQYDVQQDEyJhY2NvdW50cy5hY2Nlc3Njb250cm9sLndpbmRvd3MubmV0MB4XDTE3MDUwNjAwMDAwMFoXDTE5MDUwNzAwMDAwMFowLTErMCkGA1UEAxMiYWNjb3VudHMuYWNjZXNzY29udHJvbC53aW5kb3dzLm5ldDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJL7dVZkeJ8JDTPIzFNMJo9oEkvDc52zrdqWSOmex0E/3rBe1tfRmUe2O9l6NYuc10QzPaNxnkyIwa5698suNUPxGlEOwvm/h5oPeFcuxAWcgYYv5s589HWFrNjCF8EfOjZ4vU5oE1744EzxdmBiaGbeE8HDBXn1vK4owlcjawgQNF7KQ6LjSWr7xtARDF2IoKE4RYy0V0uVN4eAQ3zWRWWUt3cpYjTl34EZ9pO6CE8/If7noj75g0RYd/AHkIMdXBC4SlnNz+boLbba2ksIB+5z9jVILjvZwqx7i+k2filqZkVmasB/o2ChUFbdwg6c8DTOlyS/qB25dcfoCN2GF+cCAwEAAaMhMB8wHQYDVR0OBBYEFGKpXQNrF5IoxS6bL4F92+gxOJlIMA0GCSqGSIb3DQEBCwUAA4IBAQA3HgW5SoHlvvQVxqqi+mtscDZLhNfe13iG/nx8Er5il82b79RVydNs+f9sYxc4T4ctnrZu7x5e7jInJedNdAlrPorBdw+SfvKJsmiNndXugMew1FlcQTQVIFDCbziaJav8rKyMxPfeKkc1aixbajWZkKg6OPmmJn2ceTocbn8PMQy20xNvcWUwgF5FZZIuPqu6feOLJcUIYw+0JFZ265xka30QXpmytcIxajIzpD4PRdCIBuVSqgXacAs4t4+w+OhnosD72yvXck8M4GwX1j+vcuyw0yhDGNMmqsHWP7H3jnJiGDrKhhdVyplzDhTfv2Whbv/dIDn+meLE3yyC5yGL" +
                "</X509Certificate>" +
                "                </X509Data>" +
                "            </KeyInfo>" +
                "        </KeyDescriptor>" +


                "        <KeyDescriptor use=\"signing\">" +
                "            <KeyInfo>" +
                "                <X509Data>" +
                "<X509Certificate>" +
                "hello" +
                "</X509Certificate>" +
                "                </X509Data>" +
                "            </KeyInfo>" +
                "        </KeyDescriptor>" +


                "    </IDPSSODescriptor>" +
                "</EntityDescriptor>";

        FederationMetadataDocument doc = new FederationMetadataDocument(_xmlFederationMetadataDocument);
        List<FederationCertificate> certs = doc.getAllCertificates();
        assertEquals(2, certs.size());

        String certString = addCertHeaders(CertificateRawString);
        assertEquals(certString, certs.get(0).getCertificate());

        certString = addCertHeaders("hello");
        assertEquals(certString, certs.get(1).getCertificate());
    }

    @Test
    public void testGetCertificate_ZeroCertificates_ThrowsException() throws Exception {
        String _xmlFederationMetadataDocument =
                "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                        "<EntityDescriptor>" +
                        "    <IDPSSODescriptor>" +
                        "        <KeyDescriptor use=\"signing\">" +
                        "            <KeyInfo>" +
                        "            </KeyInfo>" +
                        "        </KeyDescriptor>" +
                        "    </IDPSSODescriptor>" +
                        "</EntityDescriptor>";

        FederationMetadataDocument doc = new FederationMetadataDocument(_xmlFederationMetadataDocument);

        boolean threwException = false;
        try { FederationCertificate cert = doc.getCertificate(1); threwException = false; }
        catch (NoCertificateFoundException e) { threwException = true; }

        if (!threwException)
            fail("Should have thrown an exception");
    }

    @Test
    public void testGetCertificate_OneCertificate_GetCert0_ThrowsException() throws Exception {

        FederationMetadataDocument doc = new FederationMetadataDocument(XmlFederationMetadataDocument);

        boolean threwException = false;
        try { FederationCertificate cert = doc.getCertificate(0); threwException = false; }
        catch (NoCertificateFoundException e) { threwException = true; }

        if (!threwException)
            fail("Should have thrown an exception");
    }

    @Test
    public void testGetCertificate_OneCertificate_GetCert1_GetCert() throws Exception {

        FederationMetadataDocument doc = new FederationMetadataDocument(XmlFederationMetadataDocument);

        boolean threwException = false;
        try {
            FederationCertificate cert = doc.getCertificate(1);
            threwException = false;
            assertEquals(addCertHeaders(CertificateRawString), cert.getCertificate());
        }
        catch (NoCertificateFoundException e) { threwException = true; }

        if (threwException)
            fail("Should not have thrown an exception");
    }

    @Test
    public void testGetCertificate_OneCertificate_GetCert2_ThrowsException() throws Exception {

        FederationMetadataDocument doc = new FederationMetadataDocument(XmlFederationMetadataDocument);

        boolean threwException = false;
        try {
            FederationCertificate cert = doc.getCertificate(2);
            threwException = false;
        }
        catch (NoCertificateFoundException e) { threwException = true; }

        if (!threwException)
            fail("Should have thrown an exception");
    }

    @Test
    public void testGetCertificate_TwoCertificates() throws Exception {

        String _xmlFederationMetadataDocument =
                "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>" +
                        "<EntityDescriptor>" +
                        "    <IDPSSODescriptor>" +


                        "        <KeyDescriptor use=\"signing\">" +
                        "            <KeyInfo>" +
                        "                <X509Data>" +
                        "<X509Certificate>" +
                        "MIIDBTCCAe2gAwIBAgIQZSAeaqWig4BHC1ksmNNcgjANBgkqhkiG9w0BAQsFADAtMSswKQYDVQQDEyJhY2NvdW50cy5hY2Nlc3Njb250cm9sLndpbmRvd3MubmV0MB4XDTE3MDUwNjAwMDAwMFoXDTE5MDUwNzAwMDAwMFowLTErMCkGA1UEAxMiYWNjb3VudHMuYWNjZXNzY29udHJvbC53aW5kb3dzLm5ldDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJL7dVZkeJ8JDTPIzFNMJo9oEkvDc52zrdqWSOmex0E/3rBe1tfRmUe2O9l6NYuc10QzPaNxnkyIwa5698suNUPxGlEOwvm/h5oPeFcuxAWcgYYv5s589HWFrNjCF8EfOjZ4vU5oE1744EzxdmBiaGbeE8HDBXn1vK4owlcjawgQNF7KQ6LjSWr7xtARDF2IoKE4RYy0V0uVN4eAQ3zWRWWUt3cpYjTl34EZ9pO6CE8/If7noj75g0RYd/AHkIMdXBC4SlnNz+boLbba2ksIB+5z9jVILjvZwqx7i+k2filqZkVmasB/o2ChUFbdwg6c8DTOlyS/qB25dcfoCN2GF+cCAwEAAaMhMB8wHQYDVR0OBBYEFGKpXQNrF5IoxS6bL4F92+gxOJlIMA0GCSqGSIb3DQEBCwUAA4IBAQA3HgW5SoHlvvQVxqqi+mtscDZLhNfe13iG/nx8Er5il82b79RVydNs+f9sYxc4T4ctnrZu7x5e7jInJedNdAlrPorBdw+SfvKJsmiNndXugMew1FlcQTQVIFDCbziaJav8rKyMxPfeKkc1aixbajWZkKg6OPmmJn2ceTocbn8PMQy20xNvcWUwgF5FZZIuPqu6feOLJcUIYw+0JFZ265xka30QXpmytcIxajIzpD4PRdCIBuVSqgXacAs4t4+w+OhnosD72yvXck8M4GwX1j+vcuyw0yhDGNMmqsHWP7H3jnJiGDrKhhdVyplzDhTfv2Whbv/dIDn+meLE3yyC5yGL" +
                        "</X509Certificate>" +
                        "                </X509Data>" +
                        "            </KeyInfo>" +
                        "        </KeyDescriptor>" +


                        "        <KeyDescriptor use=\"signing\">" +
                        "            <KeyInfo>" +
                        "                <X509Data>" +
                        "<X509Certificate>" +
                        "hello" +
                        "</X509Certificate>" +
                        "                </X509Data>" +
                        "            </KeyInfo>" +
                        "        </KeyDescriptor>" +


                        "    </IDPSSODescriptor>" +
                        "</EntityDescriptor>";

        FederationMetadataDocument doc = new FederationMetadataDocument(_xmlFederationMetadataDocument);

        assertEquals(addCertHeaders(CertificateRawString), doc.getCertificate(1).getCertificate());
        assertEquals(addCertHeaders("hello"), doc.getCertificate(2).getCertificate());
    }
}