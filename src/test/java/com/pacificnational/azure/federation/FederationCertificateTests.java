package com.pacificnational.azure.federation;

import org.junit.Test;

import java.security.PublicKey;
import java.security.cert.CertificateException;

import static org.junit.Assert.*;

/**
 * Created by tyler on 20/7/17.
 */
public class FederationCertificateTests {

    private static final String CertificateString = "MIIDBTCCAe2gAwIBAgIQZSAeaqWig4BHC1ksmNNcgjANBgkqhkiG9w0BAQsFADAtMSswKQYDVQQDEyJhY2NvdW50cy5hY2Nlc3Njb250cm9sLndpbmRvd3MubmV0MB4XDTE3MDUwNjAwMDAwMFoXDTE5MDUwNzAwMDAwMFowLTErMCkGA1UEAxMiYWNjb3VudHMuYWNjZXNzY29udHJvbC53aW5kb3dzLm5ldDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJL7dVZkeJ8JDTPIzFNMJo9oEkvDc52zrdqWSOmex0E/3rBe1tfRmUe2O9l6NYuc10QzPaNxnkyIwa5698suNUPxGlEOwvm/h5oPeFcuxAWcgYYv5s589HWFrNjCF8EfOjZ4vU5oE1744EzxdmBiaGbeE8HDBXn1vK4owlcjawgQNF7KQ6LjSWr7xtARDF2IoKE4RYy0V0uVN4eAQ3zWRWWUt3cpYjTl34EZ9pO6CE8/If7noj75g0RYd/AHkIMdXBC4SlnNz+boLbba2ksIB+5z9jVILjvZwqx7i+k2filqZkVmasB/o2ChUFbdwg6c8DTOlyS/qB25dcfoCN2GF+cCAwEAAaMhMB8wHQYDVR0OBBYEFGKpXQNrF5IoxS6bL4F92+gxOJlIMA0GCSqGSIb3DQEBCwUAA4IBAQA3HgW5SoHlvvQVxqqi+mtscDZLhNfe13iG/nx8Er5il82b79RVydNs+f9sYxc4T4ctnrZu7x5e7jInJedNdAlrPorBdw+SfvKJsmiNndXugMew1FlcQTQVIFDCbziaJav8rKyMxPfeKkc1aixbajWZkKg6OPmmJn2ceTocbn8PMQy20xNvcWUwgF5FZZIuPqu6feOLJcUIYw+0JFZ265xka30QXpmytcIxajIzpD4PRdCIBuVSqgXacAs4t4+w+OhnosD72yvXck8M4GwX1j+vcuyw0yhDGNMmqsHWP7H3jnJiGDrKhhdVyplzDhTfv2Whbv/dIDn+meLE3yyC5yGL";
    private static final String CertificateStringWithHeaders =
            "-----BEGIN CERTIFICATE-----\n" +
            CertificateString +
            "\n-----END CERTIFICATE-----";

    @Test
    public void testGetCertificate() throws Exception {

        FederationCertificate cert = new FederationCertificate(CertificateStringWithHeaders);
        assertEquals(CertificateStringWithHeaders, cert.getCertificate());
    }

    @Test
    public void testGetPublicKey_ValidCertificate_ReturnsPublicKey() throws Exception {
        FederationCertificate cert = new FederationCertificate(CertificateStringWithHeaders);

        boolean threwException = false;
        try {
            PublicKey pk = cert.getPublicKey();
            assertEquals("X.509", pk.getFormat());
            threwException = false;
        } catch (CertificateException ex) {
            threwException = true;
        }

        if (threwException)
            fail("Should not have thrown exception");
    }

    @Test
    public void testGetPublicKey_InvalidCert_ThrowsException() throws Exception {
        FederationCertificate cert = new FederationCertificate(CertificateString);

        boolean threwException = false;
        try {
            PublicKey pk = cert.getPublicKey();
            threwException = false;
        } catch (CertificateException ex) {
            threwException = true;
            assertEquals("Certificate is not valid", ex.getMessage());
        }

        if (!threwException)
            fail("Should have thrown exception");
    }

    @Test
    public void testIsValid_NoHeaders_InvalidCert() throws Exception {
        FederationCertificate cert = new FederationCertificate(CertificateString);
        assertEquals(false, cert.isValid());
    }

    @Test
    public void testIsValid_WithHeaders_ValidCert() throws Exception {
        FederationCertificate cert = new FederationCertificate(CertificateStringWithHeaders);
        assertEquals(true, cert.isValid());
    }
}