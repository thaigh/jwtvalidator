package com.pacificnational.azure.federation;

import org.junit.Test;

import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.net.URL;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by tyler on 20/7/17.
 */
public class MetadataReaderTests {

    private static final String Url = "https://login.microsoftonline.com/d90e89c5-9aa8-4842-978d-68a1f9f798ec/federationmetadata/2007-06/federationmetadata.xml";

    private static final String TenantId = "d90e89c5-9aa8-4842-978d-68a1f9f798ec";

    private static final String Certificate =
            "-----BEGIN CERTIFICATE-----\n" +
            "MIIDBTCCAe2gAwIBAgIQZSAeaqWig4BHC1ksmNNcgjANBgkqhkiG9w0BAQsFADAtMSswKQYDVQQDEyJhY2NvdW50cy5hY2Nlc3Njb250cm9sLndpbmRvd3MubmV0MB4XDTE3MDUwNjAwMDAwMFoXDTE5MDUwNzAwMDAwMFowLTErMCkGA1UEAxMiYWNjb3VudHMuYWNjZXNzY29udHJvbC53aW5kb3dzLm5ldDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJL7dVZkeJ8JDTPIzFNMJo9oEkvDc52zrdqWSOmex0E/3rBe1tfRmUe2O9l6NYuc10QzPaNxnkyIwa5698suNUPxGlEOwvm/h5oPeFcuxAWcgYYv5s589HWFrNjCF8EfOjZ4vU5oE1744EzxdmBiaGbeE8HDBXn1vK4owlcjawgQNF7KQ6LjSWr7xtARDF2IoKE4RYy0V0uVN4eAQ3zWRWWUt3cpYjTl34EZ9pO6CE8/If7noj75g0RYd/AHkIMdXBC4SlnNz+boLbba2ksIB+5z9jVILjvZwqx7i+k2filqZkVmasB/o2ChUFbdwg6c8DTOlyS/qB25dcfoCN2GF+cCAwEAAaMhMB8wHQYDVR0OBBYEFGKpXQNrF5IoxS6bL4F92+gxOJlIMA0GCSqGSIb3DQEBCwUAA4IBAQA3HgW5SoHlvvQVxqqi+mtscDZLhNfe13iG/nx8Er5il82b79RVydNs+f9sYxc4T4ctnrZu7x5e7jInJedNdAlrPorBdw+SfvKJsmiNndXugMew1FlcQTQVIFDCbziaJav8rKyMxPfeKkc1aixbajWZkKg6OPmmJn2ceTocbn8PMQy20xNvcWUwgF5FZZIuPqu6feOLJcUIYw+0JFZ265xka30QXpmytcIxajIzpD4PRdCIBuVSqgXacAs4t4+w+OhnosD72yvXck8M4GwX1j+vcuyw0yhDGNMmqsHWP7H3jnJiGDrKhhdVyplzDhTfv2Whbv/dIDn+meLE3yyC5yGL" +
            "\n-----END CERTIFICATE-----";

    private static final String XmlFederationMetadataDocument =
            "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>" +
            "<EntityDescriptor>" +
            "    <IDPSSODescriptor>" +
            "        <KeyDescriptor use=\"signing\">" +
            "            <KeyInfo>" +
            "                <X509Data>" +
            "<X509Certificate>" +
            "MIIDBTCCAe2gAwIBAgIQZSAeaqWig4BHC1ksmNNcgjANBgkqhkiG9w0BAQsFADAtMSswKQYDVQQDEyJhY2NvdW50cy5hY2Nlc3Njb250cm9sLndpbmRvd3MubmV0MB4XDTE3MDUwNjAwMDAwMFoXDTE5MDUwNzAwMDAwMFowLTErMCkGA1UEAxMiYWNjb3VudHMuYWNjZXNzY29udHJvbC53aW5kb3dzLm5ldDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJL7dVZkeJ8JDTPIzFNMJo9oEkvDc52zrdqWSOmex0E/3rBe1tfRmUe2O9l6NYuc10QzPaNxnkyIwa5698suNUPxGlEOwvm/h5oPeFcuxAWcgYYv5s589HWFrNjCF8EfOjZ4vU5oE1744EzxdmBiaGbeE8HDBXn1vK4owlcjawgQNF7KQ6LjSWr7xtARDF2IoKE4RYy0V0uVN4eAQ3zWRWWUt3cpYjTl34EZ9pO6CE8/If7noj75g0RYd/AHkIMdXBC4SlnNz+boLbba2ksIB+5z9jVILjvZwqx7i+k2filqZkVmasB/o2ChUFbdwg6c8DTOlyS/qB25dcfoCN2GF+cCAwEAAaMhMB8wHQYDVR0OBBYEFGKpXQNrF5IoxS6bL4F92+gxOJlIMA0GCSqGSIb3DQEBCwUAA4IBAQA3HgW5SoHlvvQVxqqi+mtscDZLhNfe13iG/nx8Er5il82b79RVydNs+f9sYxc4T4ctnrZu7x5e7jInJedNdAlrPorBdw+SfvKJsmiNndXugMew1FlcQTQVIFDCbziaJav8rKyMxPfeKkc1aixbajWZkKg6OPmmJn2ceTocbn8PMQy20xNvcWUwgF5FZZIuPqu6feOLJcUIYw+0JFZ265xka30QXpmytcIxajIzpD4PRdCIBuVSqgXacAs4t4+w+OhnosD72yvXck8M4GwX1j+vcuyw0yhDGNMmqsHWP7H3jnJiGDrKhhdVyplzDhTfv2Whbv/dIDn+meLE3yyC5yGL" +
            "</X509Certificate>" +
            "                </X509Data>" +
            "            </KeyInfo>" +
            "        </KeyDescriptor>" +
            "    </IDPSSODescriptor>" +
            "</EntityDescriptor>";

    @Test
    public void test_downloadFederationMetadataFromUrl_downloadsMockDocument() throws Exception {
        MetadataReader reader = mock(MetadataReader.class);

        URL url = new URL(Url);

        // Mock call to method and force result
        when(reader.downloadFederationMetadataFromUrl(url))
                .thenReturn(new FederationMetadataDocument(XmlFederationMetadataDocument));

        // Call method and assert result
        FederationMetadataDocument doc = reader.downloadFederationMetadataFromUrl(url);
        assertEquals(XmlFederationMetadataDocument, doc.getDocumentAsString());

        FederationCertificate cert = doc.getCertificate(1);
        assertEquals(Certificate, cert.getCertificate());

    }

    @Test
    public void test_downloadFederationMetadata_downloadsMockDocument() throws Exception {
        MetadataReader reader = mock(MetadataReader.class);

        // Mock call to method and force result
        when(reader.downloadFederationMetadata(Url))
                .thenReturn(new FederationMetadataDocument(XmlFederationMetadataDocument));

        // Call method and assert result
        FederationMetadataDocument doc = reader.downloadFederationMetadata(Url);
        assertEquals(XmlFederationMetadataDocument, doc.getDocumentAsString());

        FederationCertificate cert = doc.getCertificate(1);
        assertEquals(Certificate, cert.getCertificate());
    }

    @Test
    public void test_downloadFederationMetadataFromTenant_downloadsMockDocument() throws Exception {
        MetadataReader reader = mock(MetadataReader.class);

        // Mock call to method and force result
        when(reader.downloadFederationMetadataFromTenantId(TenantId))
                .thenReturn(new FederationMetadataDocument(XmlFederationMetadataDocument));

        // Call method and assert result
        FederationMetadataDocument doc = reader.downloadFederationMetadataFromTenantId(TenantId);
        assertEquals(XmlFederationMetadataDocument, doc.getDocumentAsString());

        FederationCertificate cert = doc.getCertificate(1);
        assertEquals(Certificate, cert.getCertificate());
    }
}




