package com.pacificnational.jwt.validator;

import com.pacificnational.azure.federation.FederationCertificate;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import net.oauth.jsontoken.JsonToken;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.util.Map;

/**
 * Validation API to validate a JSON Web Token (JWT) using Steve Wright's decode API and the official JWT Claims API
 */
public class JwtValidator {

    public Map<String, Object> validateJwt(FederationCertificate certificate, String jwtToken)
            throws CertificateException, SignatureException
    {
        PublicKey publicKey = certificate.getPublicKey();
        return validateJwt(publicKey, jwtToken);
    }

    public Map<String, Object> validateJwt(PublicKey publicKey, String jwtToken)
            throws CertificateException, SignatureException
    {
        JsonToken jt = new JwtParserFactory()
                .createParser(publicKey)
                .verifyAndDeserialize(jwtToken);

        Map<String, Object> mp = new JwtDeserialiser().decodeToken(jt);
        return mp;
    }



    public Claims extractJwtClaims(FederationCertificate certificate, String token)
            throws CertificateException
    {
        PublicKey publicKey = certificate.getPublicKey();
        return extractJwtClaims(publicKey, token);
    }

    public Claims extractJwtClaims(PublicKey publicKey, String token)
    {
        Jws<Claims> x = Jwts.parser()
                .setSigningKey(publicKey)
                .parseClaimsJws(token);

        Claims c = x.getBody();
        return c;
    }

}
