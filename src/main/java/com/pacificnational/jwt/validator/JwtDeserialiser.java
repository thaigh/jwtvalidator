package com.pacificnational.jwt.validator;

import net.oauth.jsontoken.JsonToken;

import java.util.HashMap;
import java.util.Map;

/**
 * Class to decode a deserialised JWT Token.
 * Logic implemented by Steve Wright from Mulesoft for the Mulesoft aio-jwt-verify API
 */
public class JwtDeserialiser {

    public Map<String, Object> decodeToken(JsonToken jt)
    {

        Map<String, Object> mp = new HashMap<>();

        if (jt.getParamAsPrimitive("upn") != null) {
            mp.put("upn", jt.getParamAsPrimitive("upn").getAsString());
        }
        if (jt.getParamAsPrimitive("unique_name") != null){
            mp.put("unique_name",jt.getParamAsPrimitive("unique_name").toString());
        }
        if (jt.getParamAsPrimitive("iat") != null){
            mp.put("iat",jt.getParamAsPrimitive("iat").toString());
        }
        if (jt.getParamAsPrimitive("nbf")!= null){
            mp.put("nbf",jt.getParamAsPrimitive("nbf").toString());
        }
        if (jt.getParamAsPrimitive("exp") != null){
            mp.put("exp",jt.getParamAsPrimitive("exp").toString());
        }
        if (jt.getParamAsPrimitive("ver") != null){
            mp.put("ver",jt.getParamAsPrimitive("ver").toString());
        }
        if (jt.getParamAsPrimitive("tid") != null){
            mp.put("tid",jt.getParamAsPrimitive("tid").toString());
        }
        if (jt.getParamAsPrimitive("oid") != null){
            mp.put("oid",jt.getParamAsPrimitive("oid").toString());
        }
        if (jt.getParamAsPrimitive("sub") != null){
            mp.put("sub",jt.getParamAsPrimitive("sub").toString());
        }
        if (jt.getParamAsPrimitive("given_name") != null){
            mp.put("given_name",jt.getParamAsPrimitive("given_name").toString());
        }
        if (jt.getParamAsPrimitive("family_name") != null){
            mp.put("family_name",jt.getParamAsPrimitive("family_name").toString());
        }
        if (jt.getParamAsPrimitive("name") != null){
            mp.put("name",jt.getParamAsPrimitive("name").toString());
        }
        if (jt.getParamAsPrimitive("amr") != null){
            mp.put("amr",jt.getParamAsPrimitive("amr").toString());
        }
        if (jt.getParamAsPrimitive("unique_name") != null){
            mp.put("unique_name",jt.getParamAsPrimitive("unique_name").toString());
        }
        if (jt.getParamAsPrimitive("appid") != null){
            mp.put("appid",jt.getParamAsPrimitive("appid").toString());
        }
        if (jt.getParamAsPrimitive("appidacr") != null){
            mp.put("appidacr",jt.getParamAsPrimitive("appidacr").toString());
        }
        if (jt.getParamAsPrimitive("scp") != null){
            mp.put("scp",jt.getParamAsPrimitive("scp").toString());
        }
        if (jt.getParamAsPrimitive("acr") != null){
            mp.put("acr",jt.getParamAsPrimitive("acr").toString());
        }

        return mp;
    }

}
