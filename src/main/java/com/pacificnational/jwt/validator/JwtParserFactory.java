package com.pacificnational.jwt.validator;

import com.google.common.collect.Lists;
import com.google.gson.JsonObject;
import net.oauth.jsontoken.Checker;
import net.oauth.jsontoken.JsonTokenParser;
import net.oauth.jsontoken.crypto.RsaSHA256Verifier;
import net.oauth.jsontoken.crypto.SignatureAlgorithm;
import net.oauth.jsontoken.crypto.Verifier;
import net.oauth.jsontoken.discovery.VerifierProvider;
import net.oauth.jsontoken.discovery.VerifierProviders;

import java.security.PublicKey;
import java.security.SignatureException;
import java.util.List;

/**
 * Factory to create a JWT Parser using RSA 256 Asymmetric encryption
 */
public class JwtParserFactory {

    /**
     * Creates a JWT parser using RS256 encryption
     * @param publicKey The public key of the X.509 certificate file used to create the JWT Token
     * @return The JWT Parser used to verify and deserialise the JWT Token
     */
    public JsonTokenParser createParser(PublicKey publicKey)
    {
        // Should always use Asymmetric encryption for JWTs. so use RS256 and the signature algorithm
        final Verifier hmacVerifier = new RsaSHA256Verifier(publicKey);

        VerifierProvider hmacLocator = new VerifierProvider() {
            public List<Verifier> findVerifier(String id, String key){ return Lists.newArrayList(hmacVerifier); }
        };

        VerifierProviders locators = new VerifierProviders();
        locators.setVerifierProvider(SignatureAlgorithm.RS256, hmacLocator);

        Checker checker = new Checker(){
            public void check(JsonObject payload) throws SignatureException { /* don't throw - allow anything */ }
        };

        //Ignore Audience does not mean that the Signature is ignored
        JsonTokenParser parser = new JsonTokenParser(locators, checker);
        return parser;
    }


}
