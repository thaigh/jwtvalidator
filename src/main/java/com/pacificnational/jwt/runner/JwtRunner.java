package com.pacificnational.jwt.runner;

import com.pacificnational.azure.federation.*;
import com.pacificnational.jwt.validator.*;
import io.jsonwebtoken.Claims;

import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.util.*;

/**
 * Created by tyler on 31/08/2016.
 */
public class JwtRunner {

    public static void main(String[] args)
    {
        final String federationMetadataUrl = "https://login.microsoftonline.com/d90e89c5-9aa8-4842-978d-68a1f9f798ec/federationmetadata/2007-06/federationmetadata.xml";
        final String jwtTokenString = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IjlGWERwYmZNRlQyU3ZRdVhoODQ2WVR3RUlCdyIsImtpZCI6IjlGWERwYmZNRlQyU3ZRdVhoODQ2WVR3RUlCdyJ9.eyJhdWQiOiJodHRwczovL2dyYXBoLndpbmRvd3MubmV0IiwiaXNzIjoiaHR0cHM6Ly9zdHMud2luZG93cy5uZXQvZDkwZTg5YzUtOWFhOC00ODQyLTk3OGQtNjhhMWY5Zjc5OGVjLyIsImlhdCI6MTUwMDI3MDYwOSwibmJmIjoxNTAwMjcwNjA5LCJleHAiOjE1MDAyNzQ1MDksImFjciI6IjEiLCJhaW8iOiJZMlpnWUhoMnNmbVVuWHliWXJYYUVTTzF1OWtMcHJxeTMxTDBlRFJwUjhzNktROExPWEVBIiwiYW1yIjpbInB3ZCJdLCJhcHBpZCI6IjliYTFhNWM3LWYxN2EtNGRlOS1hMWYxLTYxNzhjOGQ1MTIyMyIsImFwcGlkYWNyIjoiMCIsImVfZXhwIjoyNjI4MDAsImZhbWlseV9uYW1lIjoiSGFpZ2giLCJnaXZlbl9uYW1lIjoiVHlsZXIiLCJpcGFkZHIiOiIxMy41NC4xMjMuMTA2IiwibmFtZSI6IkhhaWdoLCBUeWxlciIsIm9pZCI6IjQ3MTU4NGQ4LTFhMjEtNDcwZC1hYTE5LTY0Yjk3NWJmOGQ3NiIsIm9ucHJlbV9zaWQiOiJTLTEtNS0yMS0zNzIwMjQyOC0yNzcwMDUwNjAtMTcxMDAzNzc1My0xODUwOSIsInBsYXRmIjoiMTQiLCJwdWlkIjoiMTAwM0JGRkQ4QkIzNUM1RCIsInNjcCI6InVzZXJfaW1wZXJzb25hdGlvbiIsInN1YiI6IlItb2kybmxndmg2UXQ0YnU3NkJwd1V4aTFnRjltV1JjYzg3R25GUnVscFEiLCJ0aWQiOiJkOTBlODljNS05YWE4LTQ4NDItOTc4ZC02OGExZjlmNzk4ZWMiLCJ1bmlxdWVfbmFtZSI6IlR5bGVyX0hhaWdoQHBhY2lmaWNuYXRpb25hbC5jb20uYXUiLCJ1cG4iOiJUeWxlcl9IYWlnaEBwYWNpZmljbmF0aW9uYWwuY29tLmF1IiwidXRpIjoiOG9aLXpFUFFvME9oeThEeTlaa2NBQSIsInZlciI6IjEuMCJ9.AgLkHFvNiaxKGjxq157MszhMjocae_luwobWCvD3OagnHXfTLVXz3Tkcxs3lbhj7VAvgtmxgJC3j-Qt2K6fmvRkBy8Yqyiaz5IlsdML-ea-8Ksxc9kFAqaYI3jlGJxA9nbPDD5mc1cEpQb5WhgLZa2TMCi8UFpcv0OxEDqtBxsFsU12HVkn7lICD_RKDrH-m_Dpw07XVBIkP7ygCjlFW5QYsoge7Vk9jYoj3kncDzmmRLYQB_BtkC4B3DcUCQSxsMkE8486zskgRMghYaNvLug5jOG5KO4f74EijU9T33F5eAyAmicd5Yj2YhDQ67OS2QpxiM-O1YW1GfHCxeqADjg";

        try {
            List<FederationCertificate> certificates = getCertificates(federationMetadataUrl);

            int i = 0;
            for(FederationCertificate cert : certificates)
            {
                System.out.println("Validating against cert " + i);
                validateJwtToken(cert, jwtTokenString);
                i++;
            }

        } catch (Exception e) { e.printStackTrace(); }
    }

    public static List<FederationCertificate> getCertificates(String federationMetadataUrl) throws Exception {
        MetadataReader reader = new MetadataReader();
        FederationMetadataDocument metadata = reader.downloadFederationMetadata(federationMetadataUrl);

        return metadata.getAllCertificates();
    }


    public static void validateJwtToken(FederationCertificate cert, String jwtToken) throws CertificateException {

        JwtValidator validator = new JwtValidator();

        try {
            Map<String, Object> mp = validator.validateJwt(cert, jwtToken);
            System.out.println(mp);
        } catch (Exception e) {
            //e.printStackTrace();
            System.err.println("Failed to validate JWT");
        }

        try {
            Claims c = validator.extractJwtClaims(cert, jwtToken);
            System.out.println(c);
        } catch (Exception e) {
            //e.printStackTrace();
            System.err.println("Failed to validate JWT");
        }
    }

}
