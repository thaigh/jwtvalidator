package com.pacificnational.azure.federation;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

/**
 * Represents an X.509 certificate downloaded via Azure Federation Metadata Document
 */
public class FederationCertificate implements Serializable {

    // Class needs to be serialisable to allow Mulesoft to store in ObjectStore
    private String certificate;

    public FederationCertificate(String certificate) { this.certificate = certificate; }

    /**
     * Gets the raw certificate string data inside this certificate
     * @return The raw certificate string text
     */
    public String getCertificate() { return this.certificate; }

    @Override
    public String toString() { return this.certificate; }

    /***
     * Generates the public key associated with this certificate used to verify if
     * JSON tokens provided by the Identity Provider were signed by this the private key that
     * corresponds to this public key
     * @return The public key for this certificate
     * @throws CertificateException The public key could not be generated from this certificate
     */
    public PublicKey getPublicKey() throws CertificateException {
        // From here: http://stackoverflow.com/a/782183/2442468
        // It appears the certificate must be properly formatted

        if (!isValid())
            throw new CertificateException("Certificate is not valid");

        // IMPROVE: Making two calls to convertToX509Certificate. Aim to reduce to one
        X509Certificate certificate = convertToX509Certificate();
        PublicKey key = certificate.getPublicKey();
        return key;
    }

    public boolean isValid()
    {
        try {
            X509Certificate certificate = convertToX509Certificate();
            certificate.checkValidity();
            return true;
        } catch (CertificateException ex) {
            return false;
        }
    }

    private X509Certificate convertToX509Certificate() throws CertificateException
    {
        InputStream inStream = new ByteArrayInputStream(this.certificate.getBytes(StandardCharsets.UTF_8));

        CertificateFactory factory = CertificateFactory.getInstance(CertificateType.X509.value());
        X509Certificate certificate = (X509Certificate)factory.generateCertificate(inStream);
        return certificate;
    }

}
