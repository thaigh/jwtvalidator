package com.pacificnational.azure.federation;

/**
 * Exception thrown when querying a Federated Metadata document for an X.509 certificate that does not exist
 */
public class NoCertificateFoundException extends Exception {

    private int certIndex;

    public int getCertIndex() { return certIndex; }

    public NoCertificateFoundException(int certIndex)
    {
        super("No certificate was found at index " + certIndex);
        this.certIndex = certIndex;
    }

    public NoCertificateFoundException(int certIndex, String message)
    {
        super(message);
        this.certIndex = certIndex;
    }

}
