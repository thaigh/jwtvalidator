package com.pacificnational.azure.federation;

/**
 * Enumerate type that lists all supported certificate types for JSON Web Tokens.
 */
public enum CertificateType {

    // Currently, only X.509 certificates are supported

    /** Use X.509 certificate to generate certificates and public keys */
    X509("X.509");

    private String value;
    private CertificateType(String type) { this.value = type; }

    /**
     * Gets the string representation of the certificate type used to pass to Java Legacy APIs
     * @return The string representation of the certificate type
     */
    public String value() { return this.value; }
}
