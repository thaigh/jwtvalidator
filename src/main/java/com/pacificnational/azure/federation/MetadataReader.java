package com.pacificnational.azure.federation;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * API to download and parse certificates from azure federated metadata documents
 */
public class MetadataReader {


    public FederationMetadataDocument downloadFederationMetadataFromTenantId(String tenantDomain)
            throws IOException, InvalidMetadataException
    {
        final String federationMetadataUrl = "https://login.microsoftonline.com/%1$s/federationmetadata/2007-06/federationmetadata.xml";
        String metadataUrl = String.format(federationMetadataUrl, tenantDomain);

        return downloadFederationMetadata(metadataUrl);
    }

    /***
     * Downloads the federation metadata document from the given URL
     * @param metaDataUrl The url to download from
     * @return The federated metadata as a string
     * @throws MalformedURLException The provided URL is invalid
     */
    public FederationMetadataDocument downloadFederationMetadata(String metaDataUrl)
            throws IOException, InvalidMetadataException
    {
        return downloadFederationMetadataFromUrl(new URL(metaDataUrl));
    }


    /***
     * Downloads the federation metadata document from the given URL
     * @param metaDataUrl The url to download from
     * @return The federated metadata as a string
     */
    public FederationMetadataDocument downloadFederationMetadataFromUrl(URL metaDataUrl)
            throws IOException, InvalidMetadataException
    {
        try (InputStream is = metaDataUrl.openStream())
        {
            String xml = IOUtils.toString(is, "UTF-8");

            // Need to trim single leading whitespace from XML as per
            // http://stackoverflow.com/a/3030913/2442468
            xml = xml.trim().replaceFirst("^([\\W]+)<","<");
            return new FederationMetadataDocument(xml);
        } catch (Exception e) {
            throw e;
        }
    }

}
