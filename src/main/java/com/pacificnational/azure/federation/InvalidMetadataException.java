package com.pacificnational.azure.federation;

/**
 * Exception to be thrown when a Federated Metadata XML document cannot be parsed
 */
public class InvalidMetadataException extends Exception {

    public InvalidMetadataException() { super("Could not parse Federated Metadata to XML Document"); }

    public InvalidMetadataException(String message) { super(message); }


}
