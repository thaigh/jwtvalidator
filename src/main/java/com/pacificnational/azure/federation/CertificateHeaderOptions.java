package com.pacificnational.azure.federation;

/**
 * Enumerated options list for how to generate certificates
 */
public enum CertificateHeaderOptions {

    /*** Add certificate headers to the certificate */
    AddHeaders

    /*** Don't add certificate headers and leave the certificate in its raw format */
    //IgnoreHeaders
}
