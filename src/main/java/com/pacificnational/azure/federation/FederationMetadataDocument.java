package com.pacificnational.azure.federation;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.*;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents an Azure Federation Metadata Document
 */
public class FederationMetadataDocument {

    private Document metadataDocument;

    public FederationMetadataDocument(String metadataString) throws InvalidMetadataException {
        this.metadataDocument = convertStringToXmlDocument(metadataString);
    }

    public FederationMetadataDocument(Document metadataDocument) { this.metadataDocument = metadataDocument; }


    public String getDocumentAsString() throws TransformerException {

        // This will generate a string from the XML Document... Thanks Java
        // https://stackoverflow.com/a/5456836
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");

        // Java XML adds a "standalone='no'" directive int he XML header.
        // Any unit tests on this method need to include this standalone directive, or they will not pass
        // You can try and remove it from the generation process by referring to https://stackoverflow.com/a/8438236

        StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(this.metadataDocument), new StreamResult(writer));
        String output = writer.getBuffer().toString();//.replaceAll("\n|\r", "");
        return output;
    }

    /***
     * Gets all available certificates in the federation metadata document
     * @return A list of all certificates in the federation metadata document
     * @throws XPathExpressionException
     */
    public List<FederationCertificate> getAllCertificates() throws XPathExpressionException {
        List<FederationCertificate> certificates = new ArrayList<>();
        int i = 1;

        // TODO: Write better method for handling end of certs list
        while (true)
        {
            try {
                FederationCertificate cert = getCertificate(i++);
                certificates.add(cert);
            } catch (NoCertificateFoundException e) {
                break;
            }
        }

        return certificates;
    }


    /***
     * Gets the certificate at the given index
     * @param certIndex The index of the certificate in the Federation Metadata Dcoument
     * @return The certificate at the given index
     * @throws XPathExpressionException The XPath expression to get the certificate was invalid
     * @throws NoCertificateFoundException No certificate was found at the given index
     */
    public FederationCertificate getCertificate(int certIndex)
            throws XPathExpressionException, NoCertificateFoundException
    {
        String cert = getCertificateAtIndex(certIndex);
        cert = addHeaders(cert);
        return new FederationCertificate(cert);
    }


    /***
     * Uses XPath expression to get the XML node for the given certificate index
     * @param certIndex The certificate index
     * @return The certificate at the given index in the metadata document
     * @throws XPathExpressionException The XPath expression could not be compiled
     * @throws NoCertificateFoundException No certificate data found at the given index
     */
    private String getCertificateAtIndex(int certIndex)
            throws XPathExpressionException, NoCertificateFoundException
    {
        // Get the node at the XPath expression corresponding to the given certificate index
        XPathExpression expr = compileXPathString(certIndex);
        NodeList nl = (NodeList) expr.evaluate(this.metadataDocument, XPathConstants.NODESET);

        // Check that certificate exists
        if (nl.item(0) == null)
            throw new NoCertificateFoundException(certIndex);

        // Return the raw contents of the certificate
        return nl.item(0).getTextContent();
    }


    /**
     * Compiles an XPath expression to query the federation metadata document for the certificate at the given index
     * @param certIndex The index of the certificate to get
     * @return An XPath expression to query the Federation Metadata XML document
     * @throws XPathExpressionException Could not compile the XPath expression
     */
    private XPathExpression compileXPathString(int certIndex) throws XPathExpressionException {
        // xpath://md:EntityDescriptor/md:IDPSSODescriptor/md:KeyDescriptor[1][@use='signing']/ds:KeyInfo/ds:X509Data/ds:X509Certificate
        final String xpathTemplate = "/EntityDescriptor/IDPSSODescriptor/KeyDescriptor[%1$d][@use='signing']/KeyInfo/X509Data/X509Certificate";
        String xpathStr = String.format(xpathTemplate, certIndex);

        XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xpath = xPathfactory.newXPath();
        XPathExpression expr = xpath.compile(xpathStr);
        return expr;
    }


    /**
     * Adds header and footer text to the raw certificate data
     * @param cert The raw certificate data
     * @return The certificate with added headers and footers
     */
    private String addHeaders(String cert)
    {
        return "-----BEGIN CERTIFICATE-----\n" +
                cert + "\n" +
                "-----END CERTIFICATE-----";
    }


    /**
     * Converts an xml string to an XML Document
     * @param xmlString The xml string
     * @return The XML Document
     * @throws InvalidMetadataException Could not parse the XML document
     */
    private Document convertStringToXmlDocument(String xmlString) throws InvalidMetadataException {

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new InputSource(new StringReader(xmlString)));
            return document;
        } catch (Exception e) {

            // Throw wrapped exception to reduce the number of possible exceptions the user has to catch
            InvalidMetadataException error = new InvalidMetadataException(e.getMessage());
            error.setStackTrace(e.getStackTrace());
            throw error;
        }
    }


}
